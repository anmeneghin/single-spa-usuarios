import { UsuarioTabelaService } from './../services/usuario-tabela.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-tabela',
  templateUrl: './tabela.component.html',
  styleUrls: ['./tabela.component.sass']
})
export class TabelaComponent implements OnInit {
  ngOnInit(): void {
    this.listarUsuarios();
  }

  displayedColumns: string[] = ['id', 'nome', 'acao'];
  dataSource!: any;
  usuarios!: any;
  subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  constructor(
    public dialog: MatDialog,
    private usuarioTabela: UsuarioTabelaService
  ) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(action: any, obj: any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Adicionar') {
        this.adicionarUsuario(result.data);
      } else if (result.event == 'Editar') {
        this.editarUsuario(result.data);
      } else if (result.event == 'Excluir') {
        this.excluirUsuario(result.data);
      }
    });
  }

  adicionarUsuario(obj: any) {
    this.subscriptions.push(
      this.usuarioTabela.adicionarUsuario(obj)
        .pipe(
          filter((response) => response.status === 201),
        )
        .subscribe(() => {
          this.listarUsuarios();
        })
    )
  }
  editarUsuario(obj: any) {
    this.subscriptions.push(
      this.usuarioTabela.atualizarUsuario(obj)
        .pipe(
          filter((response) => response.status === 200)
        )
        .subscribe(() => {
          this.listarUsuarios();
        })
    )
  }
  excluirUsuario(obj: any) {
    this.subscriptions.push(
      this.usuarioTabela.excluirUsuario(obj.id)
        .pipe(
          filter((response) => response.status === 200)
        )
        .subscribe(() => {
          this.listarUsuarios();
        })
    )
  }

  listarUsuarios(): void {
    this.subscriptions.push(
      this.usuarioTabela.listarUsuarios()
        .pipe(
          filter(response => response.status === 200),
          map(response => response.body),
          tap(response => {
            this.usuarios = response;
            this.dataSource = new MatTableDataSource(this.usuarios);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          ),
        )
        .subscribe()
    );
  }
}
