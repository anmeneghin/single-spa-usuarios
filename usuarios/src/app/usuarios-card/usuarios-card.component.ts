import { tap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioApiService } from '../shared/usuario-api.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioInterface } from '../interfaces/usuario-interface';

@Component({
  selector: 'app-usuarios-card',
  templateUrl: './usuarios-card.component.html',
  styleUrls: ['./usuarios-card.component.sass']
})
export class UsuariosCardComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  obs!: Observable<any>;
  usuarios!: any;
  dataSource!: MatTableDataSource<UsuarioInterface>;

  page = 1;
  length = 100;

  constructor(private usuarioApiService: UsuarioApiService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getCharacters();
  }

  getCharacters() {
    return this.usuarioApiService.getTodosUsuarios(this.page, this.length)
      .pipe(
        tap(response => {
          this.usuarios = response.results;
          this.dataSource = new MatTableDataSource(this.usuarios)
          this.changeDetectorRef.detectChanges();
          this.dataSource.paginator = this.paginator;
          this.obs = this.dataSource.connect();
        })
      )
      .subscribe()
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }
}
