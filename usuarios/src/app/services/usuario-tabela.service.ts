import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';

import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, delay, take, tap } from 'rxjs/operators';

import { Usuario_Tabela } from './../interfaces/usuario-tabela-interface';


@Injectable({
  providedIn: 'root'
})
export class UsuarioTabelaService {

  users: Usuario_Tabela[] | any = [];
  private readonly API = 'http://localhost:3000/usuarios';

  constructor(private httpService: HttpClient) { }

  listarUsuarios(): Observable<HttpResponse<Usuario_Tabela[]>> {
    return this.httpService.get<Usuario_Tabela[]>(
      this.API,
      {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        }),
      }
    );
  }

  adicionarUsuario(usuario: Usuario_Tabela) {
    usuario.id = this.users[this.users.length - 1] + 1;

    return this.httpService
      .post<Usuario_Tabela>(this.API, usuario, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        }),
      })
      .pipe(catchError((err) => this.handleError(err)));
  }

  atualizarUsuario(usuario: Usuario_Tabela): Observable<HttpResponse<Usuario_Tabela>> {
    const id = usuario.id;

    return this.httpService
      .put<Usuario_Tabela>(`${this.API}/${id}`, usuario, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        }),
      })
      .pipe(catchError((err) => this.handleError(err)));
  }

  excluirUsuario(id: number): Observable<HttpResponse<Usuario_Tabela>> {
    return this.httpService
      .delete<Usuario_Tabela>(`${this.API}/${id}`, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        }),
      })

      .pipe(catchError((err) => this.handleError(err)));
  }


  private handleError(error: any): Observable<any> {
    let errorMessage = '';
    // this.alertService.showAlertDanger('Erro ao carregar as Tarefas. Tente novamente mais tarde.');

    if (
      error instanceof HttpErrorResponse &&
      error.error.hasOwnProperty('message')
    ) {
      // client-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log('Tarefas Service Error: ', errorMessage);
    return throwError(errorMessage);
  }
}
