import { TestBed } from '@angular/core/testing';

import { UsuarioTabelaService } from './usuario-tabela.service';

describe('UsuarioTabelaService', () => {
  let service: UsuarioTabelaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuarioTabelaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
