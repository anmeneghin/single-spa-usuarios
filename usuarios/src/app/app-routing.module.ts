import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { HomeComponent } from './home/home.component';
import { UsuariosCardComponent } from './usuarios-card/usuarios-card.component';
import { TabelaComponent } from './tabela/tabela.component';

const routes: Routes = [
  { path: 'usuarios/home', component: HomeComponent },
  { path: 'usuarios/usuarios-card', component: UsuariosCardComponent },
  { path: 'usuarios/tabela', component: TabelaComponent },
  { path: '**', component: EmptyRouteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }]
})
export class AppRoutingModule { }
