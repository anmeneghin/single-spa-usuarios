import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioApiService {

  URL_API = 'https://randomuser.me/api/'

  constructor(private http: HttpClient) { }

  getTodosUsuarios(page: number, results: number): Observable<any> {
    return this.http.get<any>(this.URL_API + '?page=' + page + '&results=' + results)
  }
}
