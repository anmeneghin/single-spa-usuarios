import { registerApplication, start } from "single-spa";
import * as isActive from "./activity-functions";

registerApplication({
  name: "@mf-demo/teste",
  app: () => System.import("@mf-demo/teste"),
  activeWhen: ["/teste", (location) => location.pathname.startsWith("/teste")],
});
registerApplication({
  name: "@mf-demo/usuarios",
  app: () => System.import("@mf-demo/usuarios"),
  activeWhen: [
    "/usuarios",
    (location) => location.pathname.startsWith("/usuarios"),
  ],
});

// registerApplication(
//   "@mf-demo/navbar",
//   () => System.import("@mf-demo/navbar"),
//   isActive.navbar
// );

// //Config with more expressive API
// registerApplication({
//   name: "@mf-demo/produtos",
//   app: () => System.import("@mf-demo/produtos"),
//   activeWhen: ['/produtos', (location) => location.pathname.startsWith('/produtos')]
// });

// registerApplication({
//   name: "@mf-demo/usuarios",
//   app: () => System.import("@mf-demo/usuarios"),
//   activeWhen: location => location.pathname === "/usuarios"
// });

// registerApplication({
//   name: "@mf-demo/employee-details",
//   app: () => System.import("@mf-demo/employee-details"),
//   activeWhen: isActive.employeeDetails
// });

// registerApplication(
//   "@mf-demo/footer",
//   () => System.import("@mf-demo/footer"),
//   isActive.footer
// );

start();
